import { Component, OnInit, EventEmitter } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Router } from "@angular/router";
import { ModalComponent } from "../modal/modal.component";
import { LeadsService } from "src/app/services/leads.service";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"]
})
export class DashboardComponent implements OnInit {
  headerData: any;
  tokenData =
    "Bearer eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJkcmxGUDA2YUpQdldBbmEyZnhPMmR0OFhBbzdGSC0zNlJ0dmxtYlRHeUhrIn0.eyJqdGkiOiJkMDg3M2IxMS0wNDhkLTQ0YWUtYjQ2My0wOWJjZGY4NGJkNzUiLCJleHAiOjE1ODE1Mzc2NjMsIm5iZiI6MCwiaWF0IjoxNTgxNTAxNjY1LCJpc3MiOiJodHRwczovL2xvZ2luLmFjY2lvbmJyZWV6ZS5jb20vYXV0aC9yZWFsbXMvbWFzdGVyIiwiYXVkIjpbIkFjY2F0aG9uIDIwMTktcmVhbG0iLCJicm9rZXIiLCJhY2NvdW50Il0sInN1YiI6IjdiNTFlYTVjLTRjMzctNDIwOS04MjY1LWFkODQ2MmI4OWZhZiIsInR5cCI6IkJlYXJlciIsImF6cCI6ImFkbWluLWNvbnNvbGUiLCJub25jZSI6ImM1MTljNmM5LTIyNGQtNDk1ZC04NWIwLTgzNmYzMDBmNzRmNiIsImF1dGhfdGltZSI6MTU4MTUwMTY2Mywic2Vzc2lvbl9zdGF0ZSI6ImVjY2I1Mzg0LWJlMzgtNGE4Mi1iY2RkLTZiMTk2YmY4ZDFmYSIsImFjciI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOlsiKiJdLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsibmV3Iiwib2ZmbGluZV9hY2Nlc3MiLCJ1bWFfYXV0aG9yaXphdGlvbiIsImdvb2dsZS1sb2dpbiJdfSwicmVzb3VyY2VfYWNjZXNzIjp7IkFjY2F0aG9uIDIwMTktcmVhbG0iOnsicm9sZXMiOlsidmlldy1yZWFsbSIsImNyZWF0ZS1jbGllbnQiLCJ2aWV3LXVzZXJzIiwidmlldy1jbGllbnRzIiwibWFuYWdlLWNsaWVudHMiLCJxdWVyeS1jbGllbnRzIiwicXVlcnktZ3JvdXBzIiwicXVlcnktdXNlcnMiXX0sImJyb2tlciI6eyJyb2xlcyI6WyJyZWFkLXRva2VuIl19LCJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6Im9wZW5pZCBwcm9maWxlIGVtYWlsIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJuYW1lIjoiQWthc2ggVmlzaHdha2FybWEiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJha2FzaC52aXNod2FrYXJtYUBhY2Npb25sYWJzLmNvbSIsImdpdmVuX25hbWUiOiJBa2FzaCIsImZhbWlseV9uYW1lIjoiVmlzaHdha2FybWEiLCJlbWFpbCI6ImFrYXNoLnZpc2h3YWthcm1hQGFjY2lvbmxhYnMuY29tIn0.Yiv2PBo-8wS7R9r3TlXAq9ZpIqrYXVa2tVRm0wheqSbUn-8RmrSfdFxZMNKlIG7jy1XVrujlTG66xVOD9DbcuzWCzZVDsSKpzDabkb3kgg-I6emhA6NUxxV4D09mMRQc8Vxp5IxNkX0_W2v6eywaD4MnyPhKAkpZUVr6d4C0h1qViHxkMuVFG7GMHYdfYlHbYtf-E0y1xZc7L0AACELJBPb-rQJQIP6BOYIywlw_6GfEa4XLTlkHR5rT-ekr9F-KIt5U60cZgZdee5raWAbUyS6Lpj__W-j4FWK2LR8t-bPDladdenOJMqQFFIO90XfHNomiMTwkN0rtPlRxAlkmqA";
  errorMessage = "";
  submitData = {
    token: this.tokenData
  };
  refreshForm: EventEmitter<any>;
  constructor(
    private modalService: NgbModal,
    private _http: HttpClient,
    private router: Router,
    public restApi: LeadsService
  ) {}

  ngOnInit() {
    this.dataDisplay();
    this.refreshForm = new EventEmitter();
  }

  createData() {
    this.router.navigate(["/newlead"]);
  }

  dataDisplay() {
    return this.restApi.getDatatable().subscribe((data: {}) => {
      this.headerData = JSON.stringify(data);
    });
  }

  deleteRow(f) {
    if (f.detail.eventName === "row_click") {
      this.router.navigate(["/editform", f.detail.data.data.id]);
    } else if (f.detail.eventName === "deleteRow") {
      let userId = f.detail.data.id;
      let modalRef = this.modalService.open(ModalComponent);
      modalRef.componentInstance.id = userId;

      modalRef.result
        .then(result => {
          this.refreshForm.emit({
            property: "table"
          });
        })
        .catch(error => {
          return error;
        });
    }
  }
}
