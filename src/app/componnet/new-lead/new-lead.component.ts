import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { LeadsService } from "src/app/services/leads.service";

@Component({
  selector: "app-new-lead",
  templateUrl: "./new-lead.component.html",
  styleUrls: ["./new-lead.component.css"]
})
export class NewLeadComponent implements OnInit {
  formData: any;
  errorMessage = "";
  constructor(
    private _http: HttpClient,
    private router: Router,
    public restApi: LeadsService
  ) {}

  ngOnInit() {
    this.getFormData();
  }

  getFormData(): void {
    let formUrl =
      "https://breeze.accionbreeze.com/static/form-service/lead-data";
    this._http.get(formUrl).subscribe((data: any) => {
      this.formData = data;
    });
  }

  onSubmit(event) {
    this.restApi.createLead(event.data).subscribe(
      response => {
        this.router.navigate(["/dashboard"]);
      },
      error => {
        this.errorMessage = error.message;
      }
    );
  }
}
