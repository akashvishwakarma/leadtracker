import { Component, OnInit, Input } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { LeadsService } from "src/app/services/leads.service";

@Component({
  selector: "app-modal",
  templateUrl: "./modal.component.html",
  styleUrls: ["./modal.component.css"]
})
export class ModalComponent implements OnInit {
  @Input() id: number;
  errorMessage = "";
  constructor(
    public activeModal: NgbActiveModal,
    private _http: HttpClient,
    private router: Router,
    public restApi: LeadsService
  ) {}

  ngOnInit() {}

  closeModal() {
    this.activeModal.close("Modal Closed");
  }

  deleteData(id) {
    this.restApi.deleteLead(id).subscribe(
      response => {
        this.activeModal.close("Modal Closed");
        this.router.navigate(["/dashboard"]);
      },
      error => {
        this.errorMessage = error.message;
      }
    );
  }
}
