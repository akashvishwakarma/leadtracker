import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Router, ActivatedRoute } from "@angular/router";
import { Subscription } from "rxjs";
import { LeadsService } from "src/app/services/leads.service";

@Component({
  selector: "app-edit-form",
  templateUrl: "./edit-form.component.html",
  styleUrls: ["./edit-form.component.css"]
})
export class EditFormComponent implements OnInit {
  dataById: any;
  errorMessage = "";
  private routeSub: Subscription;
  constructor(
    private _http: HttpClient,
    private router: Router,
    private route: ActivatedRoute,
    public restApi: LeadsService
  ) {}

  ngOnInit() {
    this.getId();
  }

  getId() {
    let id = this.route.snapshot.paramMap.get("id");
    return this._http
      .get(`https://lead-tracking.accionbreeze.com/lead-data/${id}`)
      .subscribe((data: any) => {
        this.dataById = { data: data };
        // console.log(this.dataById, "headerDadataByIdta");
      });
  }

  onEditSubmit(edit) {
    let id = this.route.snapshot.paramMap.get("id");
    let editData = edit.data;

    this.restApi.updateLead(id, editData).subscribe(
      response => {
        this.router.navigate(["/dashboard"]);
      },
      error => {
        this.errorMessage = error.message;
      }
    );
  }
}
