import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Router, NavigationEnd } from "@angular/router";
import { Location } from '@angular/common';
@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  encapsulation: ViewEncapsulation.None,
  styleUrls: ["./header.component.css"]
})
export class HeaderComponent implements OnInit {
  modalReference = null;
  constructor(private _http: HttpClient, private route: Router, private location: Location) {
    this.route.events.subscribe((event: any) => {
      if (event instanceof NavigationEnd) {
        return this.route.url;
      }
    });
  }
  goBack() {
    this.location.back();
  }

  ngOnInit() {}
}
