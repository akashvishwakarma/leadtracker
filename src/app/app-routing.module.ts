import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewLeadComponent } from '../app/componnet/new-lead/new-lead.component';
import { DashboardComponent } from '../app/componnet/dashboard/dashboard.component';
import { EditFormComponent } from '../app/componnet/edit-form/edit-form.component';
const routes: Routes = [
  { path: 'dashboard', component: DashboardComponent },
  { path: 'newlead', component: NewLeadComponent },
  {path: 'editform/:id', component: EditFormComponent},
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
