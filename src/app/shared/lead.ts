export class Lead {
  pursuitstartdate: Date;
  businessUnit: string;
  salesOwner: string;
  clientName: string;
  opportunityStage: string;
  closuredate: Date;
  engagementType: string;
  leadSource: string;
  weightage: number;
  clientType: string;
  projectNameRequirement: string;
  currentupdateandnextsteps: string;
  technologyDomain: string;
  teamSize: number;
  estimatedBudget: number;
}
