import { BrowserModule } from "@angular/platform-browser";
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { FormioModule } from "angular-formio";
import { HeaderComponent } from "./componnet/header/header.component";
import { DashboardComponent } from "./componnet/dashboard/dashboard.component";
import { HttpClientModule } from "@angular/common/http";
import { NgxGridModule } from "ngx-web-grid";
import { ModalComponent } from "./componnet/modal/modal.component";
import { NewLeadComponent } from "./componnet/new-lead/new-lead.component";
import { EditFormComponent } from "./componnet/edit-form/edit-form.component";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DashboardComponent,
    ModalComponent,
    NewLeadComponent,
    EditFormComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    AppRoutingModule,
    FormioModule,
    HttpClientModule,
    NgxGridModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  entryComponents: [ModalComponent]
})
export class AppModule {}
